FROM python:3.9.10-alpine3.15 

ADD . /test-project
WORKDIR /test-project

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "app:app", "--preload"]

FROM denoland/deno:alpine-1.35.3 


copy app /app
WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "app:app", "--preload"]