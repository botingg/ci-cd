import { serve } from "https://deno.land/std/http/server.ts";

const server = serve({ hostname: "0.0.0.0", port: 5000 });

console.log("Server is running at http://localhost:5000/");

// 使用 async 函式處理請求
async function handleRequest(request: any) {
  await request.respond({ body: "Hello World\n" });
}

// 使用 async 函式處理請求
async function main() {
  for await (const req of server) {
    await handleRequest(req); // 等待處理請求的函式完成
  }
}

main();
